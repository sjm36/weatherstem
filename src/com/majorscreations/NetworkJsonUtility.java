package com.majorscreations;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang.StringEscapeUtils;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;


public final class NetworkJsonUtility
{
    private static String apiKey;
    private static String location;
    private static String station;

    private static final JdomParser JDOM_PARSER = new JdomParser();

    private final String USER_AGENT = "Mozilla/5.0";

    public static void setStation(String station) {
        NetworkJsonUtility.station = station;
    }

    public static void setApiKey(String apiKey) {
        NetworkJsonUtility.apiKey = apiKey;
    }

    public static void setLocation(String location) {
        NetworkJsonUtility.location = location;
    }

    public static String getStation() {
        return station;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static String getLocation() {
        return location;
    }

    public ArrayList<IndividualStationReading> getStationReadingObjectsFromJson(String jsonRequest) {

        ArrayList<IndividualStationReading> returnValue = null;
        try {
            returnValue = new ArrayList<IndividualStationReading>();
            String postResponse = doPost(jsonRequest);
            JsonRootNode json = JDOM_PARSER.parse(postResponse);

            List<JsonNode> nodes = json.getElements();

            for (JsonNode elementNodes: nodes) {

                IndividualStationReading i = new IndividualStationReading();
                // Block to fill in station information
                JsonNode station = elementNodes.getNode("station");
                setStationInformation(station, i);

                // Block to fill in records
                JsonNode record = elementNodes.getNode("record");
                i.setTimeOfReading(record.getStringValue("time"));
                List<JsonNode> recordList = record.getArrayNode("readings");



                for (JsonNode reading : recordList) {
                    insertIntoReadingSingleValue(reading, i);
                }

                returnValue.add(i);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }

    public InputStream doPostForImageStream(String url) throws Exception {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        int responseCode = con.getResponseCode();

        InputStream is = con.getInputStream();
        return is;
    }

    private String doPost(String jsonRequest) throws Exception {

            String url = "https://" + NetworkJsonUtility.location + ".weatherstem.com/api";
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "input=" + jsonRequest;
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
    }

    private String formatBearing(double bearing) {
        if (bearing < 0 && bearing > -180) {
            // Normalize to [0,360]
            bearing = 360.0 + bearing;
        }
        if (bearing > 360 || bearing < -180) {
            return "Unknown";
        }

        String directions[] = {
                "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE",
                "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW",
                "N"};
        String cardinal = directions[(int) Math.floor(((bearing + 11.25) % 360) / 22.5)];
        return cardinal;
    }

    private void setStationInformation(JsonNode node, IndividualStationReading reading) {
        reading.setStationName(node.getStringValue("name"));
        reading.setTwitter(node.getStringValue("twitter"));
        reading.setWunderground(node.getStringValue("wunderground"));
        reading.setLocation(node.getNode("domain").getStringValue("name"));
        reading.setLongitude(node.getStringValue("lon"));
        reading.setLatitude(node.getStringValue("lat"));
        List<JsonNode> cameras = node.getArrayNode("cameras");

        for (JsonNode camera: cameras) {
            reading.getCameras().put(camera.getStringValue("name"), camera.getStringValue("image"));
        }
    }

    private void insertIntoReadingSingleValue(JsonNode node, IndividualStationReading reading) {
        String sensorType = node.getStringValue("sensor_type");
        String rawValue = node.getStringValue("value");
        String unitString = StringEscapeUtils.unescapeHtml(node.getStringValue("unit_symbol"));
        String concat = "";

        if (sensorType.toLowerCase().equals("barometer tendency")) {
            reading.setBarometerTendencyReading(rawValue);
        } else if (sensorType.toLowerCase().equals("barometer")) {
            concat = rawValue + " " + unitString;
            reading.setBarometerReading(concat);
        } else if (sensorType.toLowerCase().equals("thermometer")) {
            concat = rawValue + " " + unitString;
            reading.setThermometerReading(concat);
        } else if (sensorType.toLowerCase().equals("dewpoint")) {
            concat = rawValue + " " + unitString;
            reading.setDewpointReading(concat);
        } else if (sensorType.toLowerCase().equals("heat index")) {
            concat = rawValue + " " + unitString;
            reading.setHeatIndexReading(concat);
        } else if (sensorType.toLowerCase().equals("wet bulb globe temperature")) {
            concat = rawValue + " " + unitString;
            reading.setWetBulbGlobeTempReading(concat);
        } else if (sensorType.toLowerCase().equals("wind chill")) {             // Wind chill
            concat = rawValue + " " + unitString;
            reading.setWindChillReading(concat);
        } else if (sensorType.toLowerCase().equals("anemometer")) {             // Wind speed
            concat = rawValue + " " + unitString;
            reading.setAnemometerReading(concat);
        } else if (sensorType.toLowerCase().equals("hygrometer")) {             // Humidity
            concat = rawValue + " " + unitString;
            reading.setHygrometerReading(concat);
        } else if (sensorType.toLowerCase().equals("wind vane")) {              // Wind vane (direction)
            concat = formatBearing(Double.parseDouble(rawValue));
            reading.setWindVaneReading(concat);
        } else if (sensorType.toLowerCase().equals("solar radiation sensor")) { // Solar radiation
            concat = rawValue + " " + unitString;
            reading.setSolarRadiationReading(concat);
        } else if (sensorType.toLowerCase().equals("uv radiation sensor")) {    // UV radiation
            concat = rawValue + " " + unitString;
            reading.setUvRadiationReading(concat);
        } else if (sensorType.toLowerCase().equals("rain gauge")) {             // Overall rain gauge
            concat = rawValue + " " + unitString;
            reading.setRainGaugeReading(concat);
        } else if (sensorType.toLowerCase().equals("rain rate")) {              // Rain rate
            concat = rawValue + " " + unitString;
            reading.setRainRateReading(concat);
        } else if (sensorType.toLowerCase().equals("leaf wetness indicator")) { // Leaf wetness indicator
            concat = rawValue + " " + unitString;
            reading.setcGardenLeafWetnessReading(concat);
        } else if (sensorType.toLowerCase().equals("extra temperature")) {    //Botany Greenhouse Temp
            concat = rawValue + " " + unitString;
            reading.setBotanyGreenhouseTemperature(concat);
        } else if (sensorType.toLowerCase().equals("extra humidity")) {       //Botany Greenhouse Humidity
            concat = rawValue + " " + unitString;
            reading.setBotanyGreenhouseHumidityReading(concat);
        } else if (sensorType.toLowerCase().equals("soil temperature probe")) { //Botany Soil Temp Probes
            if (node.getStringValue("sensor").toLowerCase().contains("6 inches")) { // 6 inch sensor
                concat = rawValue + " " + unitString;
                reading.setcGardenSoilTemp6inReading(concat);
            } else {                                                                // 12 inch sensor
                concat = rawValue + " " + unitString;
                reading.setcGardenSoilTemp12inReading(concat);
            }
        } else if (sensorType.toLowerCase().equals("soil moisture probe")) { // Botany soil moisture probes
            if (node.getStringValue("sensor").toLowerCase().contains("6 inches")) { // 6 inch sensor
                concat = rawValue + " " + unitString;
                reading.setcGardenSoilMoisture6inReading(concat);
            } else {                                                                // 12 inch sensor
                concat = rawValue + " " + unitString;
                reading.setcGardenSoilTemp12inReading(concat);
            }
        }
    }
}
