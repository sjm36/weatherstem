package com.majorscreations.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Stephen on 4/29/2015.
 */
public class BackgroundPanel extends JPanel {

    private BufferedImage image;
    private float alpha = 0.0f;

    public BackgroundPanel(File imageFile) {
        setImage(imageFile);
    }

    public void setImage(InputStream imageStream) {
        try {
            BufferedImage original = ImageIO.read(imageStream);
            image = this.createResizedCopy(original,1280,1024,true);

        } catch (IOException io) {
            Logger.getGlobal().log(Level.SEVERE, "Image file from stream could not be loaded.");
            image = null;
        }
    }

    public void setImage(File imageFile) {
        if (imageFile == null) {
            Logger.getGlobal().log(Level.INFO, "Null passed to BackgroundPanel. Using defaults.");
            return;
        }

        try {
            image = ImageIO.read(imageFile);
        } catch (IOException io) {
            Logger.getGlobal().log(Level.SEVERE, "Image file " + imageFile.getAbsolutePath() + " could not be loaded");
            image = null;
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (image == null) {
            g.setColor(Color.BLACK);
            g.fillRect(0,0,this.getWidth(),this.getHeight());
            g.setColor(Color.WHITE);
            g.drawString("Image loading error.", 20, 20);
        } else {
            g.drawImage(image,0,0,null);
        }
    }

    private BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }

}
