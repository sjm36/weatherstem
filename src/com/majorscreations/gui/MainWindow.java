package com.majorscreations.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import com.majorscreations.IndividualStationReading;
import com.majorscreations.NetworkJsonUtility;

import argo.jdom.JsonObjectNodeBuilder;
import argo.jdom.JsonRootNode;
import argo.format.CompactJsonFormatter;

import static argo.jdom.JsonNodeBuilders.*;
import static javax.swing.SwingConstants.*;

/**
 * Created by Stephen on 4/29/2015.
 */
public class MainWindow extends JFrame {

    private static final CompactJsonFormatter JSON_FORMATTER = new CompactJsonFormatter();

    // Insert Swing Components here
    private BackgroundPanel bgImage;
    private DataPanel dataScreen;

    private JButton aboutButton;

    private JLabel temperatureLabel;
    private JLabel windLabel;
    private JLabel locationLabel;
    private JLabel timeLabel;
    private JLabel barometerLabel;

    // Misc. Components
    private ArrayList<IndividualStationReading> readingList;
    private Timer runTask;



    public MainWindow(List<String> settings) {

        long timerDelay = Long.parseLong(settings.get(0).split("=")[1]);
        String apiKey = settings.get(1).split("=")[1];
        String location = settings.get(2).split("=")[1];
        String station = settings.get(3).split("=")[1];

        NetworkJsonUtility.setApiKey(apiKey);
        NetworkJsonUtility.setLocation(location);
        NetworkJsonUtility.setStation(station);

        System.out.println("Setting up timer execution");
        runTask = new Timer();
        runTask.schedule(new RunUpdate(),1000,timerDelay);

        // Window Metrics
        setSize(1280, 1024);
        setResizable(false);
        setTitle("WeatherSTEM Demo Application");

        setupSwingComponents();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void setupSwingComponents() {
        // Background image JPanel block
        bgImage = new BackgroundPanel(null);
        bgImage.setLayout(new GridBagLayout());
        // Central Data JPanel
        dataScreen = new DataPanel();
        dataScreen.setLayout(new BoxLayout(dataScreen, BoxLayout.PAGE_AXIS));
        dataScreen.setPreferredSize(new Dimension(800, 600));
        dataScreen.setBackground(new Color(0, 35, 0, 44));

        dataScreen.add(Box.createRigidArea(new Dimension(10,10)));               // Create a margin on the left side of the panel

        // Location label
        locationLabel = new JLabel();
        locationLabel.setFont(new Font("Arial",Font.PLAIN, 22));
        locationLabel.setForeground(new Color(0, 198, 255, 224));
        locationLabel.setOpaque(false);
        locationLabel.setHorizontalAlignment(SwingConstants.CENTER);
        //locationLabel.setAlignmentY(Component.TOP_ALIGNMENT);
        //locationLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        dataScreen.add(locationLabel);


        // Time Label
        timeLabel = new JLabel();
        timeLabel.setFont(new Font("Arial", Font.PLAIN, 18));
        timeLabel.setForeground(new Color(0,198,244,242));
        dataScreen.add(timeLabel);
        dataScreen.add(Box.createVerticalStrut(50));

        temperatureLabel = new JLabel();
        temperatureLabel.setFont(new Font("Arial",Font.BOLD, 32));
        temperatureLabel.setForeground(Color.GREEN);
        temperatureLabel.setOpaque(false);
        temperatureLabel.setHorizontalAlignment(SwingConstants.LEFT);
        //temperatureLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        //temperatureLabel.setAlignmentY(Component.RIGHT_ALIGNMENT);

        dataScreen.add(temperatureLabel);

        windLabel = new JLabel();
        windLabel.setFont(new Font("Arial",Font.PLAIN, 18));
        windLabel.setForeground(Color.GREEN);
        windLabel.setOpaque(false);
        windLabel.setHorizontalAlignment(SwingConstants.LEFT);
        //windLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        dataScreen.add(windLabel);

        barometerLabel = new JLabel();
        barometerLabel.setFont(new Font("Arial",Font.PLAIN, 18));
        barometerLabel.setForeground(Color.GREEN);
        dataScreen.add(barometerLabel);



        // Debug Stuff
        //JTextField textField = new JTextField(10);
        //dataScreen.add(textField);

        aboutButton = new JButton("About Application");
        GridBagConstraints aboutConstraints = new GridBagConstraints();
        aboutConstraints.anchor = GridBagConstraints.LAST_LINE_START;
        aboutConstraints.insets = new Insets(20,10,20,20);
        bgImage.add(aboutButton, aboutConstraints);
        aboutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"WeatherSTEM Java Application Version 0.1a\nDeveloped April 2015 By Stephen Majors\n\n" +
                        "WeatherSTEM is (c) UCompass and Edward Mansouri\n\nUses the following open source libraries:\n\n" +
                        "Argo JSON Library (http://argo.sourceforge.net/)\nApache Commons Library Version 3.2\n" +
                        "Rounded Panels (c)2010 b4rc0ll0", "About", JOptionPane.INFORMATION_MESSAGE + JOptionPane.OK_OPTION);
            }
        });

        // End Debug Stuff
        add(bgImage);
        bgImage.add(dataScreen);
    }

    private void doUpdateTasks() {
        System.out.println("Loading...");
        long startRunTime = System.currentTimeMillis();
        //NetworkJsonUtility.setApiKey("ix3fb0na");                   // The API key
        //NetworkJsonUtility.setLocation("escambia");                 // Location of sensor (county)
        //NetworkJsonUtility.setStation("uwf");                       // Station Identifier

        ArrayList<IndividualStationReading> readings;
        NetworkJsonUtility utility = new NetworkJsonUtility();
        try {
            //String jsonContent = new String(Files.readAllBytes(Paths.get("C:\\Users\\Stephen\\Desktop\\data.json")));
            //NetworkJsonUtility utility = new NetworkJsonUtility();
            JsonObjectNodeBuilder builder = anObjectBuilder()
                    .withField("api_key", aStringBuilder(NetworkJsonUtility.getApiKey()))
                    .withField("stations", anArrayBuilder().withElement(aStringBuilder("uwf")));
            JsonRootNode toSend = builder.build();
            String jsonContent = JSON_FORMATTER.format(toSend);

            readings = utility.getStationReadingObjectsFromJson(jsonContent);
        } catch (Exception ex) {
            return;
        }

        try {
            String url = readings.get(0).getCameras().get("Cloud Camera");
            InputStream is = utility.doPostForImageStream(url);
            bgImage.setImage(is);
            bgImage.repaint();
            is.close();
        } catch (MalformedURLException mf) {
            mf.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (Exception e) {

        }

        readingList = readings;


    }

    class RunUpdate extends TimerTask {


        public void run() {

            UpdateWorker worker = new UpdateWorker();
            worker.execute();
        }
    }

    class UpdateWorker extends SwingWorker {
        @Override
        protected Void doInBackground() {
            System.out.println("Doing update now...");
            doUpdateTasks();
            return null;
        }

        @Override
        protected void done() {
            System.out.println("Completed worker thread");
            temperatureLabel.setText("Temperature: " + readingList.get(0).getThermometerReading());
            windLabel.setText("Wind " + readingList.get(0).getWindVaneReading() + " at " + readingList.get(0).getAnemometerReading());
            locationLabel.setText("Current conditions at " + readingList.get(0).getStationName());
            timeLabel.setText("As of " + readingList.get(0).getTimeOfReading());
            barometerLabel.setText("Barometer: " + readingList.get(0).getBarometerReading() + " and " + readingList.get(0).getBarometerTendencyReading());
        }
    }
}
