package com.majorscreations;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by Stephen on 4/27/2015.
 */
public class IndividualStationReading {
    // Sensor readings
    private String anemometerReading;
    private String barometerReading;
    private String barometerTendencyReading;
    private String botanyGreenhouseHumidityReading;
    private String botanyGreenhouseTemperature;
    private String cGardenLeafWetnessReading;
    private String cGardenSoilMoisture12inReading;
    private String cGardenSoilMoisture6inReading;
    private String cGardenSoilTemp12inReading;
    private String cGardenSoilTemp6inReading;
    private String dewpointReading;
    private String heatIndexReading;
    private String hygrometerReading;
    private String rainGaugeReading;
    private String rainRateReading;
    private String solarRadiationReading;
    private String thermometerReading;
    private String uvRadiationReading;
    private String wetBulbGlobeTempReading;
    private String windChillReading;
    private String windVaneReading;
    private String timeOfReading;

    // Station information strings
    private String stationName;
    private String location;
    private String latitude;
    private String longitude;
    private String twitter;
    private String wunderground;
    private HashMap<String,String> cameras;


    /**
     * Constructs an individual weather station reading. All values are optional in a way. Populate them
     * with -1 for BigDecimal values, or an empty string for string values.
     * @param anemometerReading Wind speed in miles per hour
     * @param barometerReading Barometer reading to two decimal places
     * @param barometerTendencyReading The tendency string for the barometer
     * @param botanyGreenhouseHumidityReading The humidity value inside the greenhouse
     * @param botanyGreenhouseTemperature The temperature value inside the greenhouse
     * @param cGardenLeafWetnessReading Garden leaf wetness value
     * @param cGardenSoilMoisture12inReading Soil moisture at 12 inches
     * @param cGardenSoilMoisture6inReading Soil moisture at 6 inches
     * @param cGardenSoilTemp12inReading Soil temperature at 12 inches
     * @param cGardenSoilTemp6inReading Soil temperature at 6 inches
     * @param dewpointReading Dewpoint reading
     * @param heatIndexReading Heat index reading.
     * @param hygrometerReading The relative humidity
     * @param rainGaugeReading Total rainfall for the day
     * @param rainRateReading Current rainfall rate
     * @param solarRadiationReading Solar radiation in Watts/meter squared
     * @param thermometerReading Outside temperature reading
     * @param uvRadiationReading UV Index
     * @param wetBulbGlobeTempReading The wet bulb temperature
     * @param windChillReading Current wind chill value
     * @param windVaneReading The wind direction in degrees.
     */
    public IndividualStationReading(String anemometerReading, String barometerReading, String barometerTendencyReading, String botanyGreenhouseHumidityReading, String botanyGreenhouseTemperature, String cGardenLeafWetnessReading, String cGardenSoilMoisture12inReading, String cGardenSoilMoisture6inReading, String cGardenSoilTemp12inReading, String cGardenSoilTemp6inReading, String dewpointReading, String heatIndexReading, String hygrometerReading, String rainGaugeReading, String rainRateReading, String solarRadiationReading, String thermometerReading, String uvRadiationReading, String wetBulbGlobeTempReading, String windChillReading, String windVaneReading) {
        this.anemometerReading = anemometerReading;
        this.barometerReading = barometerReading;
        this.barometerTendencyReading = barometerTendencyReading;
        this.botanyGreenhouseHumidityReading = botanyGreenhouseHumidityReading;
        this.botanyGreenhouseTemperature = botanyGreenhouseTemperature;
        this.cGardenLeafWetnessReading = cGardenLeafWetnessReading;
        this.cGardenSoilMoisture12inReading = cGardenSoilMoisture12inReading;
        this.cGardenSoilMoisture6inReading = cGardenSoilMoisture6inReading;
        this.cGardenSoilTemp12inReading = cGardenSoilTemp12inReading;
        this.cGardenSoilTemp6inReading = cGardenSoilTemp6inReading;
        this.dewpointReading = dewpointReading;
        this.heatIndexReading = heatIndexReading;
        this.hygrometerReading = hygrometerReading;
        this.rainGaugeReading = rainGaugeReading;
        this.rainRateReading = rainRateReading;
        this.solarRadiationReading = solarRadiationReading;
        this.thermometerReading = thermometerReading;
        this.uvRadiationReading = uvRadiationReading;
        this.wetBulbGlobeTempReading = wetBulbGlobeTempReading;
        this.windChillReading = windChillReading;
        this.windVaneReading = windVaneReading;
    }

    public IndividualStationReading() {
        this.anemometerReading = String.valueOf(-1.0);
        this.barometerReading = String.valueOf(-1.0);
        this.barometerTendencyReading = "null";
        this.botanyGreenhouseHumidityReading = String.valueOf(-1.0);
        this.botanyGreenhouseTemperature = String.valueOf(-1.0);
        this.cGardenLeafWetnessReading = String.valueOf(-1.0);
        this.cGardenSoilMoisture12inReading = String.valueOf(-1.0);
        this.cGardenSoilMoisture6inReading = String.valueOf(-1.0);
        this.cGardenSoilTemp12inReading = String.valueOf(-1.0);
        this.cGardenSoilTemp6inReading = String.valueOf(-1.0);
        this.dewpointReading = String.valueOf(-1.0);
        this.heatIndexReading = String.valueOf(-1.0);
        this.hygrometerReading = String.valueOf(-1.0);
        this.rainGaugeReading = String.valueOf(-1.0);
        this.rainRateReading = String.valueOf(-1.0);
        this.solarRadiationReading = String.valueOf(-1.0);;
        this.thermometerReading = String.valueOf(-1.0);
        this.uvRadiationReading = String.valueOf(-1.0);
        this.wetBulbGlobeTempReading = String.valueOf(-1.0);
        this.windChillReading = String.valueOf(-1.0);
        this.windVaneReading = String.valueOf(-1.0);

        this.location = "null";
        this.cameras = new HashMap<String,String>();
        this.latitude = String.valueOf(0.000);
        this.longitude = String.valueOf(0.000);
        this.twitter = "null";
        this.wunderground = "null";
        this.stationName = "null";
    }

    public String getAnemometerReading() {
        return anemometerReading;
    }

    public void setAnemometerReading(String anemometerReading) {
        this.anemometerReading = anemometerReading;
    }

    public String getBarometerReading() {
        return barometerReading;
    }

    public void setBarometerReading(String barometerReading) {
        this.barometerReading = barometerReading;
    }

    public String getBarometerTendencyReading() {
        return barometerTendencyReading;
    }

    public void setBarometerTendencyReading(String barometerTendencyReading) {
        this.barometerTendencyReading = barometerTendencyReading;
    }

    public String getBotanyGreenhouseHumidityReading() {
        return botanyGreenhouseHumidityReading;
    }

    public void setBotanyGreenhouseHumidityReading(String botanyGreenhouseHumidityReading) {
        this.botanyGreenhouseHumidityReading = botanyGreenhouseHumidityReading;
    }

    public String getBotanyGreenhouseTemperature() {
        return botanyGreenhouseTemperature;
    }

    public void setBotanyGreenhouseTemperature(String botanyGreenhouseTemperature) {
        this.botanyGreenhouseTemperature = botanyGreenhouseTemperature;
    }

    public String getcGardenLeafWetnessReading() {
        return cGardenLeafWetnessReading;
    }

    public void setcGardenLeafWetnessReading(String cGardenLeafWetnessReading) {
        this.cGardenLeafWetnessReading = cGardenLeafWetnessReading;
    }

    public String getcGardenSoilMoisture12inReading() {
        return cGardenSoilMoisture12inReading;
    }

    public void setcGardenSoilMoisture12inReading(String cGardenSoilMoisture12inReading) {
        this.cGardenSoilMoisture12inReading = cGardenSoilMoisture12inReading;
    }

    public String getcGardenSoilMoisture6inReading() {
        return cGardenSoilMoisture6inReading;
    }

    public void setcGardenSoilMoisture6inReading(String cGardenSoilMoisture6inReading) {
        this.cGardenSoilMoisture6inReading = cGardenSoilMoisture6inReading;
    }

    public String getcGardenSoilTemp12inReading() {
        return cGardenSoilTemp12inReading;
    }

    public void setcGardenSoilTemp12inReading(String cGardenSoilTemp12inReading) {
        this.cGardenSoilTemp12inReading = cGardenSoilTemp12inReading;
    }

    public String getcGardenSoilTemp6inReading() {
        return cGardenSoilTemp6inReading;
    }

    public void setcGardenSoilTemp6inReading(String cGardenSoilTemp6inReading) {
        this.cGardenSoilTemp6inReading = cGardenSoilTemp6inReading;
    }

    public String getDewpointReading() {
        return dewpointReading;
    }

    public void setDewpointReading(String dewpointReading) {
        this.dewpointReading = dewpointReading;
    }

    public String getHeatIndexReading() {
        return heatIndexReading;
    }

    public void setHeatIndexReading(String heatIndexReading) {
        this.heatIndexReading = heatIndexReading;
    }

    public String getHygrometerReading() {
        return hygrometerReading;
    }

    public void setHygrometerReading(String hygrometerReading) {
        this.hygrometerReading = hygrometerReading;
    }

    public String getRainGaugeReading() {
        return rainGaugeReading;
    }

    public void setRainGaugeReading(String rainGaugeReading) {
        this.rainGaugeReading = rainGaugeReading;
    }

    public String getRainRateReading() {
        return rainRateReading;
    }

    public void setRainRateReading(String rainRateReading) {
        this.rainRateReading = rainRateReading;
    }

    public String getSolarRadiationReading() {
        return solarRadiationReading;
    }

    public void setSolarRadiationReading(String solarRadiationReading) {
        this.solarRadiationReading = solarRadiationReading;
    }

    public String getThermometerReading() {
        return thermometerReading;
    }

    public void setThermometerReading(String thermometerReading) {
        this.thermometerReading = thermometerReading;
    }

    public String getUvRadiationReading() {
        return uvRadiationReading;
    }

    public void setUvRadiationReading(String uvRadiationReading) {
        this.uvRadiationReading = uvRadiationReading;
    }

    public String getWetBulbGlobeTempReading() {
        return wetBulbGlobeTempReading;
    }

    public void setWetBulbGlobeTempReading(String wetBulbGlobeTempReading) {
        this.wetBulbGlobeTempReading = wetBulbGlobeTempReading;
    }

    public String getWindChillReading() {
        return windChillReading;
    }

    public void setWindChillReading(String windChillReading) {
        this.windChillReading = windChillReading;
    }

    public String getWindVaneReading() {
        return windVaneReading;
    }

    public void setWindVaneReading(String windVaneReading) {
        this.windVaneReading = windVaneReading;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getWunderground() {
        return wunderground;
    }

    public void setWunderground(String wunderground) {
        this.wunderground = wunderground;
    }

    public HashMap<String, String> getCameras() {
        return cameras;
    }

    public void setCameras(HashMap<String, String> cameras) {
        this.cameras = cameras;
    }

    public void setTimeOfReading(String timeOfReading) {
        this.timeOfReading = timeOfReading;
    }

    public String getTimeOfReading() {
        return timeOfReading;
    }


}
