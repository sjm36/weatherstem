package com.majorscreations;

import com.majorscreations.gui.MainWindow;

import javax.swing.*;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

public class Main {


    private static final String configfile = "settings.cfg";            // Settings filename
    /*
     * The configuration file must be in this format:
     * delay=xxx
     * apikey=xxxxx
     * location=xxxxx
     * station=xxxxx
     */

    public static void main(String[] args) throws Exception {

        // Set look and feel the the current system
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        // Load settings file. If not loaded, go ahead and exit.
        List<String> settings = configurationFileStringRepresentation();
        if (settings == null)
        {
            JOptionPane.showMessageDialog(null, "The configuration file does not exist or does is not in the correct path.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
            return;

        }
        new MainWindow(settings);

    }

    private static List<String> configurationFileStringRepresentation() {

        List<String> returnString;

        try {
            returnString = Files.readAllLines(FileSystems.getDefault().getPath("cfg", configfile), Charset.defaultCharset());
        } catch (IOException io) {
            return null;
        }
        return returnString;
    }
}
