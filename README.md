If you do not know what WeatherSTEM is, please follow this [link](http://www.weatherstem.com) to find out more information. The short version of it is that WeatherSTEM was developed to get more young people interested in the STEM fields by way of weather stations all around the globe. Applications can be easily developed for the platform.

# **Okay, so what exactly is this repository for anyway?** #

I decided to go ahead and use the skills I have been taught in college, to develop this application. Our college, [The University of West Florida](http://www.uwf.edu), installed a WeatherSTEM system in April of 2015. I immediately became intrigued due to my love of weather. So I began work on this application.

# **What exactly is this supposed to display** #

It displays weather information for the most parts.

# **Information** #

The application uses the Argo JSON library to parse the JSON data: [http://argo.sourceforge.net](http://argo.sourceforge.net)
It also uses Apache Spring Commons Library 2.6

# **What needs to be done to run** #

You will most importantly need a configuration file. This file is named in a static variable in the Main entry point. The default location will be /mainapplication/cfg/settings.cfg

It has the following format:


```
#!java

delay=xxxx
apikey=xxxx
location=xxxx
station=xxxx
```


delay is the amount of time in milliseconds that the application will wait before making another request to the webserver
The other three values are values you can obtain by signing up for WeatherSTEM, and getting your API key.

# **What I am looking for help on** #

UI design! Designing good looking UI's has never been a strong point of mine. Swing is a nightmare to work with sometimes.